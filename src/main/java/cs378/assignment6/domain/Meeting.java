package cs378.assignment6.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "meetings")
public class Meeting implements Serializable {

    @Id
    private String name;

    @Id
    private String link;

    @Id
    private int year;

    @Id
    private String fk_projects_name;

    @Id
    private String description;

    public Meeting() {
    }

    public Meeting(String name, String link, int year, String fk_projects_name, String description) {
        this.name = name;
        this.link = link;
        this.year = year;
        this.fk_projects_name = fk_projects_name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public int getYear() {
        return year;
    }
}
