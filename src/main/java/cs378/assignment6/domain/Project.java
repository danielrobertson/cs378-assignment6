package cs378.assignment6.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "projects")
public class Project implements Serializable {

    @Id
    private String name;
    @Id
    private String description;

    public Project() {
        // this constructor used by Hibernate
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
