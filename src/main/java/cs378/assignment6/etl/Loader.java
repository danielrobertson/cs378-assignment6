package cs378.assignment6.etl;

import cs378.assignment6.domain.Meeting;

import java.util.ArrayList;

public interface Loader {
	public void load(ArrayList<Meeting> meetings) throws Exception;
}
