package cs378.assignment6.etl.impl;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.etl.Transformer;

import java.util.ArrayList;

public class BasicTransformerImpl implements Transformer {

    /** Transform the list of meeting names into Meeting objects */
	public ArrayList<Meeting> transform(ArrayList<String> meetingNames) throws Exception {
        ArrayList<Meeting> meetings = new ArrayList<Meeting>();
        String projectName = "solum";
        int year = 2014;
        String localHost = "http://localhost:8080/myeavesdrop/projects/solum/meetings/";
        String description = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/2014/";

        for(String name: meetingNames) {
            Meeting m = new Meeting(name, localHost + name, year, projectName, description + name);
            meetings.add(m);
        }
		return meetings;
	}
}
