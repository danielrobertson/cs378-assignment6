package cs378.assignment6.etl;

import cs378.assignment6.domain.Meeting;

import java.util.ArrayList;

public interface Transformer {
	public ArrayList<Meeting> transform(ArrayList<String> source) throws Exception;
}
