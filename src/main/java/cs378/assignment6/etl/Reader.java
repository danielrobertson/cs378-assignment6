package cs378.assignment6.etl;

import java.util.ArrayList;

public interface Reader {
	public ArrayList<String> read(String source) throws Exception;
}
