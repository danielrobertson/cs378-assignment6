package cs378.assignment6.service;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.domain.Project;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import cs378.assignment6.etl.Loader;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class MeetingDataMgrService implements Loader {
    private SessionFactory sessionFactory;

    public MeetingDataMgrService() {
        // A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
    }

    /** Insert project Solum into the projects table */
    public void insertSolumProject() {
        // insert Solum into projects table
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(new Project("solum", "solum is a project"));
        session.getTransaction().commit();
        session.close();
    }

    /** Query the database and return all meetings */
    public List read() throws Exception {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Meeting.class).
                add(Restrictions.eq("fk_projects_name", "solum"));
        return criteria.list();
    }

    @Override
    public void load(ArrayList<Meeting> meetings) throws Exception {
        insertMeetingRecords(meetings);
    }

    /** Insert the Meetings into the database using Hibernate */
    private void insertMeetingRecords(ArrayList<Meeting> meetings) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        for(Meeting m: meetings) {
            session.save(m);
        }

        session.getTransaction().commit();
        session.close();
    }

    /** Convert the list of Meetings into a XML response to be rendered on a web browser */
    public String buildResponseXml(ArrayList<Meeting> meetings) {
        String result = null;
        // build XML for project and its associated meetings
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // project tag
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("project");
            doc.appendChild(rootElement);

            // project name
            Element nameElement = doc.createElement("name");
            nameElement.appendChild(doc.createTextNode("solum"));
            rootElement.appendChild(nameElement);

            // project description
            Element descriptionElement = doc.createElement("description");
            descriptionElement.appendChild(doc.createTextNode("solum is a project"));
            rootElement.appendChild(descriptionElement);

            // meetings tag
            Element meetingsElement = doc.createElement("meetings");
            rootElement.appendChild(meetingsElement);

            for(Meeting m: meetings) {
                // meeting tag
                Element meetingElement = doc.createElement("meeting");
                meetingsElement.appendChild(meetingElement);

                // meeting name
                Element meetingNameElement = doc.createElement("name");
                meetingNameElement.appendChild(doc.createTextNode(m.getName()));
                meetingElement.appendChild(meetingNameElement);

                // meeting link
                Element meetingLinkElement = doc.createElement("link");
                meetingLinkElement.appendChild(doc.createTextNode(m.getLink()));
                meetingElement.appendChild(meetingLinkElement);

                // meeting year
                Element meetingYearElement = doc.createElement("year");
                meetingYearElement.appendChild(doc.createTextNode(Integer.toString(m.getYear())));
                meetingElement.appendChild(meetingYearElement);
            }

            result = getStringFromDocument(doc);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return result;

    }

    /** Convert Document to String */
    public String getStringFromDocument(Document doc)
    {
        try
        {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        }
        catch(TransformerException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
}
