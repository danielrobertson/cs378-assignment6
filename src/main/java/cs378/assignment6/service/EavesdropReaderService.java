package cs378.assignment6.service;

import cs378.assignment6.etl.Reader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class EavesdropReaderService implements Reader {

    /** Fetch and return all the meeting names from the Solum project page on OpenStack using JSoup */
	public ArrayList<String> read(String source) throws Exception {
        ArrayList<String> meetingNames = new ArrayList<String>();

        Document document = Jsoup.connect(source).get();
        if(document != null) {
            Elements items = document.select("tr td a");

            String name;
            for(Element item: items) {
                if (item.toString().contains("Parent Directory"))
                    continue;

                name = item.text();
                meetingNames.add(name);
            }
        }
        return meetingNames;
    }
}
