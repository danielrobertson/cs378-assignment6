package cs378.assignment6.resources;

import cs378.assignment6.controller.ETLController;
import cs378.assignment6.domain.Meeting;
import cs378.assignment6.service.MeetingDataMgrService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.ArrayList;

@Path("/projects")
public class MeetingInfoResource {

	private ETLController etlController;
	private MeetingDataMgrService meetingDataMgrService;
	
	public MeetingInfoResource() {
		etlController = new ETLController();
		meetingDataMgrService = new MeetingDataMgrService();
        meetingDataMgrService.insertSolumProject();
		
		// Start data load by spinning up an asynchronous thread, rather than manually starting the ETLController
        (new Thread(etlController)).start();
	}
	
	// Test method
	@GET
	@Path("/solum/getAll")
	public String getAll() {
		return "Hello, world";
	}
	
	@GET
	@Path("/solum/meetings")
	@Produces("application/xml")
	public String getMeetingList() {
		String response = "";
		try {
            ArrayList<Meeting> meetings = (ArrayList<Meeting>) meetingDataMgrService.read();
            response = meetingDataMgrService.buildResponseXml(meetings);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

}
