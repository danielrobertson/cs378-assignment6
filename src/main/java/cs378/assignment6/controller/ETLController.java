package cs378.assignment6.controller;

import java.util.ArrayList;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.etl.Loader;
import cs378.assignment6.etl.Reader;
import cs378.assignment6.etl.Transformer;
import cs378.assignment6.etl.impl.BasicTransformerImpl;
import cs378.assignment6.service.EavesdropReaderService;
import cs378.assignment6.service.MeetingDataMgrService;

public class ETLController implements Runnable {

	private Reader eavesdropReader;
	private Transformer transformer;
	private Loader meetingDataMgr;
	
	public ETLController() {
		eavesdropReader = new EavesdropReaderService();
		transformer = new BasicTransformerImpl();
		meetingDataMgr = new MeetingDataMgrService();
	}
	
	private void performETLActions() {
		try {
			String source = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/2014/";

            // 1) Read
            ArrayList<String> meetingNames = eavesdropReader.read(source);
			
			// 2) Transform
            ArrayList<Meeting> meetings = transformer.transform(meetingNames);
			
			// 3) Load
			meetingDataMgr.load(meetings);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    /** Two options to run the ETLController.
     * main() but this isn't ideal - we'll use this for the project
     * Spin up a thread that calls this run() method  - preferred method in real world because
     * the source may continue to produce data that you need to persist
     */

	public static void main(String[] args) {
		ETLController etlController = new ETLController();
		etlController.performETLActions();
	}

	public void run() {
        /* This will be a threaded object */
		performETLActions();
	}
}
